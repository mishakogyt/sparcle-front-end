const gulp = require('gulp');
const browserSync = require('browser-sync');
const sass = require('gulp-sass');
const cleanCSS = require('gulp-clean-css');
const autoprefixer = require('gulp-autoprefixer');
const rename = require("gulp-rename");
const imagemin = require('gulp-imagemin');
const htmlmin = require('gulp-htmlmin');

gulp.task('server', function () {

   browserSync({
      server: {
         baseDir: "dist"
      }
   });

   gulp.watch("src/*.html").on('change', browserSync.reload);
});

gulp.task('styles', function () {
   return gulp.src("src/assets/sass/**/*.+(scss|sass)")
      .pipe(sass({
         outputStyle: 'compressed'
      }).on('error', sass.logError))
      .pipe(rename({
         suffix: '.min',
         prefix: ''
      }))
      .pipe(autoprefixer())
      .pipe(cleanCSS({
         compatibility: 'ie8'
      }))
      .pipe(gulp.dest("dist/assets/css"))
      .pipe(gulp.dest("src/assets/css"))
      .pipe(browserSync.stream());
});

gulp.task('watch', function () {
   gulp.watch("src/assets/sass/**/*.+(scss|sass|css)", gulp.parallel('styles'));
   gulp.watch("src/*.html").on('change', gulp.parallel('html'));
   gulp.watch("src/assets/img/**/*").on('change', gulp.parallel('images'));

});

gulp.task('html', function () {
   return gulp.src("src/*.html")
      .pipe(htmlmin({
         collapseWhitespace: true
      }))
      .pipe(gulp.dest("dist/"));
});

gulp.task('scripts', function () {
   return gulp.src("src/js/**/*.js")
      .pipe(gulp.dest("dist/js"));
});

gulp.task('css', function () {
   return gulp.src("src/assets/css/**/*.css")
      .pipe(gulp.dest("dist/assets/css/"));
});

gulp.task('fonts', function () {
   return gulp.src("src/assets/fonts/**/*")
      .pipe(gulp.dest("dist/assets/fonts/"));
});

gulp.task('icons', function () {
   return gulp.src("src/assets/icons/**/*")
      .pipe(gulp.dest("dist/assets/icons/"));
});

gulp.task('mailer', function () {
   return gulp.src("src/assets/mailer/**/*")
      .pipe(gulp.dest("dist/assets/mailer"));
});

gulp.task('images', function () {
   return gulp.src("src/assets/img/**/*")
      .pipe(imagemin())
      .pipe(gulp.dest("dist/assets/img/"));
});

gulp.task('slick', function () {
   return gulp.src("src/assets/slick/**/*")
      .pipe(imagemin())
      .pipe(gulp.dest("dist/assets/slick/"));
});

gulp.task('default', gulp.parallel('watch', 'server', 'styles', 'scripts', 'fonts', 'icons', 'mailer', 'html', 'slick',
   'images', 'css'));